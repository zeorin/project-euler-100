-- The prime factors of 13195 are 5, 7, 13 and 29.
-- What is the largest prime factor of the number 600851475143 ?

module LargestPrimeFactor where

primeFactors :: Int -> [Int]
primeFactors 1 = []
primeFactors x
  | factors == []   = [x]
  | otherwise       = factors ++ primeFactors (x `div` (head factors))
  where factors = take 1 (filter (\y -> (x `mod` y) == 0) [2..x-1])

result = maximum (primeFactors 600851475143)
