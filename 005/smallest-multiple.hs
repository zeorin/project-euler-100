-- 2520 is the smallest number that can be divided by each of the numbers from
-- 1 to 10 without any remainder.
--
-- What is the smallest positive number that is evenly divisible by all of the
-- numbers from 1 to 20?

module SmallestMultiple where

isMultiple :: Int -> Int -> Bool
isMultiple x y = mod x y == 0

isMultipleOf1to20 :: Int -> Bool
isMultipleOf1to20 x = all (isMultiple x) [20,19..1]

result = head (filter isMultipleOf1to20 [1..])
