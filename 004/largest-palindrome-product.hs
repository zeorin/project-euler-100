-- A palindromic number reads the same both ways. The largest palindrome made
-- from the product of two 2-digit numbers is 9009 = 91 × 99.
--
-- Find the largest palindrome made from the product of two 3-digit numbers.

module LargestPalindromeProduct where

isPalindrome :: Int -> Bool
isPalindrome x = a == b
    where
        a = show x
        b = reverse a

listProd :: [Int] -> [Int] -> [Int]
listProd xs ys = [ (x * y) | x <- xs, y <- ys ]

result = maximum (filter isPalindrome (listProd [1..999] [1..999]))
